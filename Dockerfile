FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# For php and nodejs versions: https://github.com/monicahq/monica/blob/main/.tool-versions

RUN apt-get remove -y php-* php8.1-* libapache2-mod-php8.1 && \
    apt-get autoremove -y && \
    add-apt-repository --yes ppa:ondrej/php && \
    apt update && \
    apt install -y php8.2 php8.2-{apcu,bcmath,bz2,cgi,cli,common,curl,dba,dev,enchant,fpm,gd,gmp,gnupg,imagick,imap,interbase,intl,ldap,mailparse,mbstring,mysql,odbc,opcache,pgsql,phpdbg,pspell,readline,redis,snmp,soap,sqlite3,sybase,tidy,uuid,xml,xmlrpc,xsl,zip,zmq} libapache2-mod-php8.2 && \
    apt install -y php-{date,pear,twig,validate} && \
    rm -rf /var/cache/apt /var/lib/apt/lists

RUN wget https://getcomposer.org/download/2.0.11/composer.phar -O /usr/bin/composer && chmod +x /usr/bin/composer

ARG NODE_VERSION=20.12.2
RUN mkdir -p /usr/local/node-${NODE_VERSION} && curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

# renovate: datasource=github-releases depName=monicahq/monica versioning=semver extractVersion=^v(?<version>.+)$
ARG MONICA_VERSION=4.1.2

RUN wget https://github.com/monicahq/monica/archive/v${MONICA_VERSION}.tar.gz -O -| tar -xz -C /app/code --strip-components=1 && \
    chown -R www-data:www-data /app/code

RUN chmod -R g+w bootstrap/cache
RUN sudo -u www-data composer install --no-interaction --no-suggest --no-dev && \
    sudo -u www-data composer clear-cache

RUN yarn install && yarn run production && chown -R www-data:www-data /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/monica.conf /etc/apache2/sites-enabled/monica.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod rewrite

# Note that the sessions stuff is unused because monica uses lavarel sessions (storage/framework/sessions)
RUN crudini --set /etc/php/8.2/apache2/php.ini PHP upload_max_filesize 25M && \
    crudini --set /etc/php/8.2/apache2/php.ini PHP post_max_size 25M && \
    crudini --set /etc/php/8.2/apache2/php.ini PHP memory_limit 64M

RUN ln -s /app/data/php.ini /etc/php/8.2/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.2/cli/conf.d/99-cloudron.ini

RUN chmod -R g+rw /app/code/storage \
    && mv /app/code/storage /app/code/storage.template && ln -s /app/data/storage /app/code/storage \
    && mv /app/code/bootstrap/cache /app/code/bootstrap/cache.template && ln -s /run/monica/bootstrap-cache /app/code/bootstrap/cache \
    && ln -s /app/data/env /app/code/.env \
    && ln -s /app/code/storage/app/public /app/code/public/storage

ADD env.production.template start.sh /app/code/

CMD [ "/app/code/start.sh" ]
