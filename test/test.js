#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Monica', function () {
    this.timeout(0);

    const email = 'test@cloudron.io';
    const password = 'Changeme?1234';

    var browser;
    var app;

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 100000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function register () {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/register');

        await waitForElement(By.id('email'));
        await browser.findElement(By.id('email')).sendKeys(email);
        await browser.findElement(By.id('first_name')).sendKeys('Johnny');
        await browser.findElement(By.id('last_name')).sendKeys('Appleseed');
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.id('password_confirmation')).sendKeys(password);
        await browser.findElement(By.id('policy')).click();
        await browser.findElement(By.xpath('//button[text()="Register"]')).click();
    }

    async function login () {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.id('email'));
        await browser.findElement(By.id('email')).sendKeys(email);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Login"]')).click();
        await waitForElement(By.xpath('//h2[text()="Welcome to your account!"]'));
    }

    async function addJournalEntry () {
        await browser.get('https://' + app.fqdn + '/journal/add');
        await waitForElement(By.id('field-entry'));
        await browser.findElement(By.id('field-entry')).sendKeys('Gin and tonic');
        await browser.findElement(By.xpath('//button[text()="Save"]')).click();
        await browser.sleep(3000);
    }

    async function checkJournalEntry () {
        await browser.get('https://' + app.fqdn + '/journal');
        await waitForElement(By.xpath('//p[text()="Gin and tonic"]'));
    }

    it('install app',  function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can register', register);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id, EXEC_ARGS); });

    it('can login', login);
    it('can add journal entry', addJournalEntry);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('can check journal entry', checkJournalEntry);

    it('move to different location', function () { execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS); });
    it('can get new app information', getAppInfo);

    it('can login', login);
    it('can check journal entry', checkJournalEntry);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test updates
    it('can install app', function () { execSync('cloudron install --appstore-id com.monicahq.cloudronapp --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can register', register);
    it('can add journal entry', addJournalEntry);

    it('can update', function () { execSync('cloudron update --app ' + app.id, EXEC_ARGS); });

    it('can login', login);
    it('can check journal entry', checkJournalEntry);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});

