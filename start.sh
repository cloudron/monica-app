#!/bin/bash

set -eu

mkdir -p /run/monica/sessions /run/monica/bootstrap-cache /run/monica/framework-cache /run/monica/logs /run/monica/cache

readonly ARTISAN="sudo -E -u www-data php /app/code/artisan"

if [[ ! -f /app/data/.cr ]]; then
    echo "=> First run"
    # Taken from installation docs: https://github.com/monicahq/monica/blob/master/docs/installation/providers/generic.md
    mkdir -p /app/data/storage
    cp -R /app/code/storage.template/* /app/data/storage
    cp /app/code/env.production.template /app/data/env

    chown -R www-data:www-data /run/monica /app/data

    echo "=> Generating app key"
    $ARTISAN key:generate --force --no-interaction

    echo "=> Run migrations and seed database"
    $ARTISAN setup:production --force

    echo "=> Create the access tokens required for the API"
    $ARTISAN passport:keys --force
    $ARTISAN passport:client --personal --no-interaction

    touch /app/data/.cr
else
    echo "=> Existing installation. Running migration script"
    chown -R www-data:www-data /run/monica /app/data
    $ARTISAN monica:update --force

    if [[ ! -f /app/data/.moveavatarphotos ]]; then
        echo "=> Doing one-time migration of photos"
        $ARTISAN monica:moveavatarstophotosdirectory --force --no-interaction
        touch /app/data/.moveavatarphotos
    fi
fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

# sessions, logs and cache
[[ -d /app/data/storage/framework/sessions ]] && rm -rf /app/data/storage/framework/sessions
ln -sf /run/monica/sessions /app/data/storage/framework/sessions
rm -rf /app/data/storage/framework/cache && ln -s /run/monica/framework-cache /app/data/storage/framework/cache
rm -rf /app/data/storage/logs && ln -s /run/monica/logs /app/data/storage/logs

echo "=> Starting Apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

